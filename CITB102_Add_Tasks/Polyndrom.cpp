
#include <iostream>

using namespace std;

int main(int argc, const char * argv[]) {
    string input;
    cin>>input;
    bool isPolyndrom = true;
    for (int i = 0; i <= input.length()/2; i++) {
        if (input[i] != input[input.length()-1-i])
            isPolindrom = false;
    }
    if (isPolyndrom)
        cout << "yes\n" <<endl;
    else
        cout << "no\n" <<endl;
    return 0;
}
