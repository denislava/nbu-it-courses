#include <iostream>
#include <string>
#include <math.h>

using namespace std;

class Triangle
{
private:
    double a,b,c;
public:
    Triangle (int aSide, int bSide, int cSide)
    {
        a = aSide;
        b = bSide;
        c = cSide;
    }
    bool exists()
    {
        return (a+b>c && a+c>b && b+c>a);
    }
    double get_perimeter()
    {
        return a+b+c;
    }
    double get_area()
    {
        double semiperimeter = (a+b+c)/2;
        return sqrt(semiperimeter*(semiperimeter-a)*(semiperimeter-b)*(semiperimeter-c));
    }
};

int main()
{
    double a, b, c;
    cin >> a >> b >> c;
    
    Triangle t(a, b, c);
    
    if(!t.exists())
    {
        cout << "No such triangle!" << endl;
        return 0;
    }
    
    cout << t.get_perimeter() << endl;
    cout << t.get_area() << endl;
    
    return 0;
}
