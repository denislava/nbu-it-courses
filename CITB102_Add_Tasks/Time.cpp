#include <iostream>
#include <iomanip>
#include <string>
#include <algorithm>

using namespace std;

class Time
{
private:
    int h;
    int m;
    int s;
public:
    Time()
    {
        time_t now;
        time(&now);
        tm* t = localtime(&now);
        
        h=t->tm_hour;
        m=t->tm_min;
        s=t->tm_sec;
    }
    
    Time(int hour, int minute, int seconds)
    {
        h = hour % 24;
        m = minute % 60;
        s = seconds % 60;
    }
    
    Time(int seconds)
    {
        h=0;
        m=0;
        s=0;
        add_seconds(seconds);
    }
    
    void print()
    {
        //for homework format 00:00:00, see iomanip's setw and setfill
        cout  <<setfill('0') << setw(2) <<h << ":";
        cout <<setfill('0') << setw(2)<< m << ":" ;
        cout <<setfill('0') << setw(2)<< s << endl;
    }
    
    void add_seconds(int seconds)
    {
        s += seconds;
        int minutes = s/60;
        s = s%60;
        add_minutes(minutes);
    }
    
    void add_minutes(int minutes)
    {
        m += minutes;
        int hours = m/60;
        m = m%60;
        h = (h + hours)%24;
    }
    
    int get_hour()
    {
        return h;
    }
    
    void set_hour(int hour)
    {
        h = hour%24;
    }
    
    int seconds_from(Time t)
    {
        //for homework, improve
        return ((h-t.h)*3600 + (m-t.m)*60 + (s-t.s));
    }
    
    Time time_from(Time t)
    {
        int seconds = (h*3600+m*60+s);
        int tSeconds = (t.h*3600+t.m*60+t.s);
        int diff = seconds-tSeconds;
        return Time(diff);
    }
};

int main()
{
    Time now;
    now.print();//hh:mm:ss 23:04:58
    
    //now.add_seconds(3665);
    //now.print();
    
    Time deadline(11, 10, 0);
    deadline.print();
    cout << deadline.seconds_from(now) << endl;
    
    Time left = deadline.time_from(now);
    left.print();
    
    
    return 0;
}
