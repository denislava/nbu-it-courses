#include <iostream>
#include <string>
#include <math.h>
using namespace std;

class Circle
{
private:
    double r;
public:
    Circle(double radius): r(radius)
    {
    }
    
    double get_radius()
    {
        return r;
    }
    
    double get_perimeter()
    {
        return M_PI * r * 2;
    }
    double get_area()
    {
        return M_PI * r * r;
    }
};

int main()
{
    double radius;
    cin >> radius;
    
    Circle c(radius);
    
    cout << c.get_radius() << endl;
    cout << c.get_perimeter();
    cout << c.get_area();
    
    return 0;
}

