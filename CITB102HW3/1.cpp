/***
FN:F74334
PID:1
GID:3
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;

double happyQuotinet (vector<double> &first, vector<double> &second) {
    
    double happyQuotinet = 0;
    for (int i = 0; i < first.size(); i++) {
        happyQuotinet += first[i]/second[i];
    }
    return happyQuotinet;
    
}

int main()
{
    int vectorCapacity;
    vector <double> firstVector, secondVector;
    
    
    cin >> vectorCapacity;
    firstVector.reserve(vectorCapacity);
    secondVector.reserve(vectorCapacity);
    
    double number;
    for (int i = 0; i < vectorCapacity; i++) {
        cin >> number;
        firstVector.push_back(number);
    }
    for (int i = 0; i < vectorCapacity; i++) {
        cin >> number;
        secondVector.push_back(number);
    }

    cout << happyQuotinet(firstVector, secondVector) << endl;
	return 0;
}
