/***
FN:F74334
PID:2
GID:3
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;

void concatenateVectors (vector<double> &first, vector<double> &second) {
    
    for (int i = 0; i < second.size(); i++) {
        first.push_back(second[i]);
    }
}
int main()
{
    int firstVectorSize, secondVectorSize;
    
    vector <double> first, second;
    
    cin >> firstVectorSize;
    first.reserve(firstVectorSize);
    
    double number;
    for (int i = 0; i < firstVectorSize; i++) {
        cin >> number;
        first.push_back(number);
    }

    cin >> secondVectorSize;
    second.reserve(secondVectorSize);
    
    for (int i = 0; i < secondVectorSize; i++) {
        cin >> number;
        second.push_back(number);
    }
    
    concatenateVectors(first, second);
    
    for (int i = 0; i < first.size(); i++) {
        cout << first[i] << " ";
    }
	return 0;
}
