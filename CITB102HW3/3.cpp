/***
 FN:F74334
 PID:3
 GID:3
 */

#include <iostream>
#include <string>
#include <vector>

using namespace std;

void printVector(vector <double> &v) {
    for (int i = 0; i < v.size(); i++) {
        cout << v[i] << " ";
    }
}
void shiftLeft(vector <double> &v) {
    for (int i = 1; i < v.size(); i++) {
        swap(v[i],v[i-1]);
    }
}
void reverseVector(vector <double> &v) {
    for (int i = 0; i < v.size()/2; i++) {
        swap(v[i],v[v.size()-1-i]);
    }
}

int main()
{
    int vectorSize;
    cin >> vectorSize;
    
    vector <double> v;
    
    double number;
    for (int i = 0; i < vectorSize; i++) {
        cin >> number;
        v.push_back(number);
    }
    
    string input;
    
    while (cin >> input) {
        
        //ADD
        if (input == "add") {
            int position;
            cin >> position;
            double numberToInsert;
            cin >> numberToInsert;
            
            if (position > v.size())
                v.push_back(numberToInsert);
            else
                v.insert(v.begin()+position-1, numberToInsert);
        }
        //REMOVE
        if (input == "remove") {
            int position;
            cin >> position;
            v.erase(v.begin()+position-1);
        }
        //SHIFT_LEFT
        if (input == "shift_left") {
            shiftLeft(v);
        }
        //REVERSE
        if (input == "reverse") {
            reverseVector(v);
        }
        //PRINT
        if (input == "print") {
            printVector(v);
        }
        //EXIT
        if (input == "exit") {
            break;
        }
    }
    return 0;
}










