//Denislava Shentova
//F74334
//Komplekt 3

#include <iostream>

using namespace std;

double average (int number) {
    
    double sum = 0;
    double average;
    
    
    string numberStr = to_string(number);
    
    for (int i = 0; i < numberStr.length(); i++) {
        char digit = numberStr[i];
        sum += atof(&digit);
    }
    
    average = sum / numberStr.length();
    return average;
}

int main() {
    
    int num;
    
    cin >> num;
    cout << average(num) << endl;
}
