//Denislava Shentova
//F74334
//Komplekt 3

#include <iostream>

using namespace std;

string addTabAndChangeGrade(string fNum, char grade){
    
    string newGrade;
    switch (grade) {
        case 'A':
            newGrade = "6.00";
            break;
        case 'B':
            newGrade = "5.00";
            break;
        case 'C':
            newGrade = "4.00";
            break;
        case 'D':
            newGrade = "3.00";
            break;
        case 'E':
            newGrade = "2.50";
            break;
        case 'F':
            newGrade = "2.00";
            break;
        default:
            break;
    }
    string result = fNum + "\t" + newGrade;
    
    return result;
}

int main () {
    
    string fNum, result;
    char grade;
    
    while (cin>>fNum>>grade) {
        result = addTabAndChangeGrade(fNum, grade);
        cout << result << endl;
    }
}
