//Denislava Shentova
//F74334
//Komplekt 3

#include <iostream>

using namespace std;

int main() {
    
    int n;
    double current;
    double max = INT_MIN;
    
    cin >> n;
    
    for (int i = 0; i < n; i++) {
        cin >> current;
        if (current > max) {
            max = current;
        }
    }    
    cout << max << endl;
    
    return 0;
}

