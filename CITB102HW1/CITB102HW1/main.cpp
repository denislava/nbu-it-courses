//Denislava Shentova
//F74334
//Komplekt:3


#include <iostream>
#include <math.h>
using namespace std;

void task1() {
    
    int input;
    
    cout << "Enter a number:" <<endl;
    cin >> input;
    cout << input << "^2 = " << pow(input,2) << ", " <<input << "^3 = " << pow(input,3)
    << ", " <<input << "^4 = " << pow(input,4) << ", " <<input << "^5 = " << pow(input,5) <<endl;
    
}

void task2() {
    
    double firstGrade, secondGrade;
    
    do {
        cout << "Enter grade 1:" <<endl;
        cin >> firstGrade;
        if (firstGrade !=2 && firstGrade != 3 && firstGrade !=4 && firstGrade !=5 && firstGrade != 6) {
            cout << "This grade is not valid in Bulgaria" << endl;
    	}
	}
    while (firstGrade !=2 && firstGrade != 3 && firstGrade !=4 && firstGrade !=5 && firstGrade != 6);
    
    do {
        cout << "Enter grade 2:" <<endl;
        cin >> secondGrade;
        if (secondGrade !=2 && secondGrade != 3 && secondGrade !=4 && secondGrade !=5 && secondGrade != 6) {
            cout << "This grade is not valid in Bulgaria" << endl;
    	}
    }
    while (secondGrade !=2 && secondGrade != 3 && secondGrade !=4 && secondGrade !=5 && secondGrade != 6);
    
    cout << "Your average grade is " << (firstGrade + secondGrade) / 2;
}

void task3() {
    
    double firstOperand, secondOperand, result;
    char operation;

    do {
    cout << "Enter operation(+/-/x/:)" <<endl;
    cin >> operation;
    }
    while (operation != '+' && operation != '-' && operation != 'x' && operation != ':');
    
    cout << "Enter operand 1:" <<endl;
    cin >> firstOperand;
    cout << "Enter operand 2:" <<endl;
    cin >> secondOperand;
    
    if (operation == '+') {
        result = firstOperand + secondOperand;
    }
    else if (operation == '-') {
        result = firstOperand - secondOperand;
    }
    else if (operation == 'x') {
        result = firstOperand * secondOperand;
    }
    else {
        result = firstOperand / secondOperand;
    }
    cout << firstOperand << " " << operation << " " << secondOperand << " = " << result;
}

int main(int argc, const char * argv[]) {
    int input;
    
    cout << "Choose task (1/2/3):" <<endl;
    
    do {
        cin >> input;
    }
    while (input != 1 && input != 2 && input !=3);
    switch (input) {
        case 1:
            task1();
            break;
        case 2:
            task2();
            break;
        case 3:
            task3();
            break;
        default:
            break;
    }
    return 0;
}
